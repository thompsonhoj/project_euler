-- Find the sum of even-valued Fibonacci numbers less than four million

--first, we'll grab the sum_list implemenation we used from problem_1
--sum_list sums all of the values in a list, recursively
sum_list :: [Int] -> Int

--sum_list of an empty list is 0
sum_list [] = 0

--sum_list of a non-empty list is the head plus the sum of the remaining list
sum_list (x:xs) = x + sum_list xs

--next_fib returns the next fibonacci given a list of fibonaccis
--we don't actually check that the list is of legal fibonaccis
next_fib :: [Int] -> Int
next_fib [] = 1
next_fib [1] = 1
next_fib (x0:x1:xs) = x0+x1

--fibs_to_sub generates a list of fibonaccis up to some max given a max and a list of fibonaccis, recursively
fibs_to_sub :: Int -> [Int] -> [Int]
fibs_to_sub max array | (next_fib array) >= max = array
                      | otherwise = fibs_to_sub max (next_fib array: array)

--fibs_to generates a list of fibonaccis up to some max
fibs_to :: Int -> [Int]
fibs_to x = fibs_to_sub x []

--remove_odds removes all odd values from a list
remove_odds :: [Int] -> [Int]
remove_odds [] = []
remove_odds (x:xs) | x `mod` 2 == 0 = (x:remove_odds xs)
                   | otherwise = remove_odds xs

--solve
main = print . sum_list . remove_odds . fibs_to $ 4000000
