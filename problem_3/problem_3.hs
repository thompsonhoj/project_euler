-- what is the largest prime factor of the number 600851475143?

--is prime tells us if a number is prime
is_prime :: Int -> Bool
is_prime 1 = True
is_prime a = is_prime_sub a (a-1)

is_prime_sub :: Int -> Int -> Bool
is_prime_sub a 1 = True
is_prime_sub a f | a `mod` f == 0 = False
                 | otherwise = is_prime_sub a (f-1)

--next_prime will calculate the next prime number
next_prime :: Int -> Int
next_prime a | is_prime (a + 1) == True = a + 1
             | otherwise = next_prime (a + 1)

--factors takes a number and returns a list of factors
factors :: Int -> [Int]
factors a = factors_sub a 2

factors_sub :: Int -> Int -> [Int]
factors_sub l f | l == f = [f]
                | l `mod` f == 0 = (f : factors_sub (l `div` f) f)
                | otherwise = factors_sub l (next_prime f)

--solve (using the prelude's maximum function)
main = print . maximum . factors $ 600851475143
