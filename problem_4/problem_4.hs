-- find the largest palindrome made from the product of two 3-digit numbers

--digitize turns the integer into an array of single digits
arrayize :: Int -> [Int]
arrayize 0 = []
arrayize a = (a `mod` 10) : (arrayize (a `div` 10))

--palindromes --> let's just take the palindromes in the range that we want to explore: from 100^2 to 999^2
--why does the following hang...I though lazy evaluation would work here?
--palindromes = [x | x <- [1..], (reverse . arrayize $ x) == (arrayize $ x), x >= (100 * 100), x <= (999 * 999)]
palindromes = [x | x <- [(100 * 100)..(999 * 999)], (reverse . arrayize $ x) == (arrayize $ x), x >= (100 * 100), x <= (999 * 999)]

--ipo23dn --> is product of 2 3 digit numbers
ipo23dn :: Int -> Bool
ipo23dn x = ipo23dn_sub x 999

ipo23dn_sub :: Int -> Int -> Bool
ipo23dn_sub n d | d < 100 = False
                | ((n `mod` d) == 0) && ((n `div` d) > 99) && ((n `div` d) < 1000) = True
                | otherwise = ipo23dn_sub n (d-1)

--we can use this to check the 2 3 digit numbers...
ipo23dn_ns :: Int -> (Int,Int)
ipo23dn_ns x = ipo23dn_ns_sub x 999

ipo23dn_ns_sub :: Int -> Int -> (Int,Int)
ipo23dn_ns_sub n d | d < 550 = (0,0) 
                   | ((n `mod` d) == 0) && ((n `div` d) > 99) && ((n `div` d) < 1000) = (d,(n `div` d))
                   | otherwise = ipo23dn_ns_sub n (d-1)


palindromes_ipo23dn = map ipo23dn palindromes
zipped = zip palindromes palindromes_ipo23dn

find_largest_true :: [(Int, Bool)] -> Int

find_largest_true [] = 0
find_largest_true a | (snd . last $ a) == True = (fst . last $ a)
                    | otherwise = find_largest_true . take ((length a) - 1) $ a

main = print . find_largest_true $ zipped
