-- Find the sum of all multiples of 3 or 5 below 1000

--multiples below one thousand
mb1k :: Int -> [Int]
mb1k n = [x | x <- [0..999], x `mod` n == 0]

--multiples of 3 below 1k
mof3b1k = mb1k 3

--multiples of 5 below 1k
mof5b1k = mb1k 5

--now we need to merge the two ordered lists, recursively
merge_ord :: ([Int],[Int]) -> [Int]

--merge_ord for two empty lists yields empty
merge_ord ([],[]) = []

--merge_ord for an empty list and a non-empty list yields the non-empty list, in full
merge_ord ([],(y:ys)) = y:ys
merge_ord ((x:xs),[]) = x:xs

--merge_ord for two non-empty lists will yield the lesser of the two heads and the merge_ord of the remaining lists
merge_ord ((x:xs),(y:ys)) | x == y    = x : merge_ord(xs,ys)
                          | x < y     = x : merge_ord(xs,(y:ys))
                          | otherwise = y : merge_ord((x:xs),ys)

--merge mof3b1k and mof5b1k
mof3or5b1k = merge_ord(mof3b1k,mof5b1k)

--sum_list sums all of the values in a list, recursively
sum_list :: [Int] -> Int

--sum_list of an empty list is 0
sum_list [] = 0

--sum_list of a non-empty list is the head plus the sum of the remaining list
sum_list (x:xs) = x + sum_list xs

--sum the multiples of 3 or 5 below 1000
sumofmof3or5b1k = sum_list mof3or5b1k

--print the result (hopefully the answer)
main = print sumofmof3or5b1k
