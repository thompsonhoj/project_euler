-- let's just use the prelude's lcm to solve...
-- this is about a simple of a recursion pattern as i can come up with
lcm_up_to :: Int -> Int
lcm_up_to u = lcm_up_to_sub u 1

--lcm_up_to_sub takes the up_to, cur_lcm
lcm_up_to_sub :: Int -> Int -> Int
lcm_up_to_sub u c | u == 1 = c
                  | otherwise = lcm_up_to_sub (u-1) (lcm c u)

main = print . lcm_up_to $ 20
