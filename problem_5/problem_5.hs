--the following works find for 10 but struggles immensely for 20...
--sols = [1..]
--is_solution :: Int -> Bool
--is_solution a = is_solution_sub a 10
--is_solution_sub :: Int -> Int -> Bool
--is_solution_sub a 1 = True
--is_solution_sub a b | (a `mod` b == 0) = is_solution_sub a (b-1)
--                    | otherwise = False
--solve :: [Int] -> [Int]
--solve [] = []
--solve (x:xs) | is_solution x == True = x : solve xs
--             | otherwise = solve xs
--
--
-- instead, let's try to do the factorization method
--
-- actually, let's just use the prelude's lcm to solve...
-- this is about a simple of a recursion pattern as i can come up with
lcm_up_to :: Int -> Int
lcm_up_to u = lcm_up_to_sub u 1

--lcm_up_to_sub takes the up_to, cur_lcm
lcm_up_to_sub :: Int -> Int -> Int
lcm_up_to_sub u c | u == 1 = c
                  | otherwise = lcm_up_to_sub (u-1) (lcm c u)
