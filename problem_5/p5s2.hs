-- let's just use the prelude's lcm to solve...
-- this is the same recursion but using where instead of a new function to implement tail recursion
lcm_up_to :: Int -> Int
lcm_up_to u = lcm_up_to_rec u 1
        where lcm_up_to_rec 1 c = c
              lcm_up_to_rec u c = lcm_up_to_rec (u-1) (lcm c u)

main = print . lcm_up_to $ 20
