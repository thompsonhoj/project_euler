--the following works find for 10 but struggles immensely for 20; that is, until we trim the space to explore by multiples
--of the product or prime factors up to 20
sols = [0,(1*2*3*5*7*11*13*17*19)..]
is_solution :: Integer -> Bool
is_solution a = is_solution_sub a 20
is_solution_sub :: Integer -> Integer -> Bool
is_solution_sub a 1 = True
is_solution_sub a b | (a `mod` b == 0) = is_solution_sub a (b-1)
                    | otherwise = False
solve :: [Integer] -> [Integer]
solve [] = []
solve (x:xs) | is_solution x == True = x : solve xs
             | otherwise = solve xs

main = print ( (take 2 (solve sols)) !! 1 )
